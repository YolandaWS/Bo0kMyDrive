Name of group members:
-Seto Adhi Prasetyo
-Mohammad Faraz Abisha Mirza
-Yolanda Wintari Sirait
-Muhammad Rizky Andrian 
gitlab:https://gitlab.com/YolandaWS/Bo0kMyDrive
Story/description about application and its features:
This website allows the visitor to rent cars with ease.
List of features that are going to be implemented:
-The ability to look at the available cars
-The ablility to review your bookings
-The ability to look at reviews of the cars available
-The ability to look at Alex's profile

## Pipeline and Coverage
[![coverage report](https://gitlab.com/YolandaWS/Bo0kMyDrive/badges/master/coverage.svg)](https://gitlab.com/YolandaWS//-/commits/master)
[![pipeline report](https://gitlab.com/YolandaWS/Bo0kMyDrive/badges/master/pipeline.svg)](https://gitlab.com/YolandaWS//-/commits/master)

## URL
To access the website, go to [https://bookmydrive.herokuapp.com//](https://bookmydrive.herokuapp.com/)