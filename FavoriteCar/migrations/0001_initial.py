# Generated by Django 3.0.3 on 2020-05-15 13:48

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mobil',
            fields=[
                ('car', models.CharField(max_length=130, primary_key=True, serialize=False)),
                ('tipe', models.CharField(max_length=40)),
                ('rating', models.CharField(max_length=20)),
                ('owner', models.CharField(max_length=200)),
                ('year', models.IntegerField()),
                ('form', models.CharField(max_length=20)),
                ('seat', models.IntegerField()),
                ('door', models.IntegerField()),
                ('pic', models.CharField(max_length=400)),
                ('price', models.CharField(max_length=20)),
            ],
        ),
    ]
