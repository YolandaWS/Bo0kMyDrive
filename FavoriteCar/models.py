from django.db import models

class Mobil(models.Model):
    car = models.CharField(max_length=130, primary_key=True)
    tipe = models.CharField(max_length=40)
    rating = models.CharField(max_length=20)
    owner = models.CharField(max_length=200)
    year = models.IntegerField()
    form = models.CharField(max_length=20)
    seat = models.IntegerField()
    door = models.IntegerField()
    pic = models.CharField(max_length=400)
    price = models.CharField(max_length=20)

# Create your models here.
