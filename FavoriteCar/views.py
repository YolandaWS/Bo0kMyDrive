from django.shortcuts import render
from django.http import HttpResponse, QueryDict
import json
from .models import Mobil
from django.views.decorators.csrf import csrf_exempt

def FavoriteCarPage(request):
	return render(request, 'FavoriteCar.html')

def FavoriteCar(request):
	cars = Mobil.objects.all()
	return HttpResponse(cars)

@csrf_exempt
def addFavorite(request):
    mobil, created = Mobil.objects.get_or_create(
        car=request.POST['car'],
        tipe=request.POST['type'],
        rating = request.POST['rating'],
	    owner = request.POST['owner'],
	    year = request.POST['year'],
	    form = request.POST['form'],
	    seat = request.POST['seat'],
	    door = request.POST['door'],
	    pic = request.POST['pic'],
	    price = request.POST['price'],
    )
    mobil.save()
    return HttpResponse(mobil)

