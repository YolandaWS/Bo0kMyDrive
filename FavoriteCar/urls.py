from django.urls import path
from .views import FavoriteCarPage

urlpatterns = [
    path('',FavoriteCarPage,name='FavoriteCar'),
]