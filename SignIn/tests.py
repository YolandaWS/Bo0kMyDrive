from django.test import TestCase
from .views import register
from .forms import UserRegisterForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time
import random

from django.test import Client
from django.urls import resolve


from django.contrib.auth.models import User


class GAUnitTest(TestCase):

# URL
    def test_SignIn_url_exist(self):
        response = Client().get('/SignIn/')
        self.assertEqual(response.status_code, 200)

    def test_LogIn_url_exist(self):
        response = Client().get('/LogIn/')
        self.assertEqual(response.status_code, 200)

    def test_LogOut_url_exist(self):
        response = Client().get('/LogOut/')
        self.assertEqual(response.status_code, 200)

    def test_template_for_signin(self):
        response =Client().get('/SignIn/')
        self.assertTemplateUsed(response,'SignIn/SignIn.html')

    def test_template_for_login(self):
        response =Client().get('/LogIn/')
        self.assertTemplateUsed(response,'SignIn/LogIn.html')

    def test_template_for_logout(self):
        response =Client().get('/LogOut/')
        self.assertTemplateUsed(response,'SignIn/LogOut.html')

    def test_signin_function(self):
        found = resolve('/SignIn/')
        self.assertEqual(found.func, register)
    
    def test_forms_name_function(self):
        form = UserRegisterForm(data={'full_name': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['full_name'],["This field is required."])

    def test_forms_email_function(self):
        form = UserRegisterForm(data={'email': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['email'],["This field is required."])


class GAFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('chromedriver', chrome_options=chrome_options)
        super(GAFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(GAFunctionalTest, self).tearDown()

    def test_home(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/SignIn')
        self.assertIn('Sign In',self.selenium.title)

    def test_signin_process(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/SignIn')
        time.sleep(3)

        # Find the form
        name = selenium.find_element_by_id('id_full_name')
        username = selenium.find_element_by_id('id_username')
        password1 = selenium.find_element_by_id('id_password1')
        password2 = selenium.find_element_by_id('id_password2')
        email = selenium.find_element_by_id('id_email')

        # Fill the form, valid
        name.send_keys("idk")
        username.send_keys("fntest") 
        password1.send_keys('12345678ok')
        password2.send_keys('12345678ok')
        email.send_keys('shgiwr@gmail.com')

        # Submit, create account
        signinbtn = selenium.find_element_by_id('signbutton')
        signinbtn.click()

        # Should be redirected to login

        # time.sleep(3)
        # username.send_keys("fntest") 
        # password1.send_keys('12345678ok')
        # loginbtn = selenium.find_element_by_id('logbutton')
        # loginbtn.click()

        # # Should be redirected to home
        # selenium.implicitly_wait(2)
        # self.assertIn('Home',self.selenium.title)

        # # Logout from dashboard
        # selenium.implicitly_wait(2)
        # logout = selenium.find_element_by_id('logoutbutton')
        # logout.click()

        # # Back to Get Started
        # title = selenium.find_element_by_id('logout')
        # strangerGreeting = title.get_attribute('innerHTML') == "You are logged out."
        # self.assertTrue(strangerGreeting)
