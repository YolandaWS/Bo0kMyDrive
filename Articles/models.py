from django.db import models

class Articles(models.Model):
    Title = models.CharField(max_length=100)
    Content = models.TextField()
    date=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return '%s' % self.Title
        
class AddArticles(models.Model):
    Title = models.CharField(max_length=100)
    Content = models.TextField()
    short_desc = models.TextField(default="article")
    date = models.DateTimeField(auto_now_add=True)
    Rating_Choices=(("1","The Worst!"),("2","Bad"),("3","OK"),("4","Good"),("5","Awesome!"))
    Ratings = models.CharField(choices=Rating_Choices,default="1",null="True",max_length=5)
    Article = models.TextField()

    
    def __str__(self):
        return '%s' % self.Title