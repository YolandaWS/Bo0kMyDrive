from django.urls import path
from .views import Articles,AddArticles

urlpatterns = [
    path('', Articles,name='Articles'),
    path('', AddArticles, name='AddArticles')
]