from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from django.test import LiveServerTestCase
from django.db import models
import time
from selenium import webdriver
import unittest

# Create your tests here.
def test_lab6_profile_contain_name(self):
    title = 'AVAILABLE CARS'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name2(self):
    title = 'Search by Type'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name3(self):
    title = 'Search by Max Price/Day'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name4(self):
    title = 'Copyright 2020 YolandaWS'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name5(self):
    title = 'BookMyDrive'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name6(self):
    title = 'TOYOTA YARIS'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name7(self):
    title = 'NISSAN SERENA'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name8(self):
    title = 'NISSAN JUKE'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name9(self):
    title = 'MITSUBISHI XPANDER'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name10(self):
    title = 'Rating 4.7/5'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name11(self):
    title = 'Owner:Michael Muhammad'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name12(self):
    title = 'Year:2017'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name13(self):
    title = 'Type:Hatchback'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name14(self):
    title = 'Seat:5-Seater'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name15(self):
    title = 'Door:5'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_lab6_profile_contain_name16(self):
    title = 'Rp 201.000/day'
    response = Client().get('/BookACar/')
    html_response = response.content.decode('utf8')
    self.assertIn(title, html_response)
def test_URLexists(self):
    response = Client().get('/BookACar/')
    self.assertEqual(response.status_code,200)
def test_rightTemplate(self):
    response = Client().get('/BookACar/')
    self.assertTemplateUsed(response, 'BookACar.html')
def test_lab6_using_index_func(self):
    found = resolve('/BookACar/')
    self.assertNotEqual(found.func, 'BookACar')