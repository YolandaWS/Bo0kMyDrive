from django import forms
from django.forms import ModelForm
from .models import AddArticle

class AddArticles(forms.ModelForm):
    class Meta:
        model = AddArticles
        fields = '__all__'

