from django.shortcuts import render
from django.http import HttpResponseRedirect

def Articles(request):
    return render(request,'Articles/Articles.html')
    
def AddArticles(request):
    AddArticles=AddArticles.objects.all().order_by('-date')
    if request.method=="POST":
        form=AddArticles(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/AddArticles')
    else:
        form=AddArticles()
    
    return render(request,'Artciles/AddArticles.html',{'form':form, 'Articles':AddArticles})
