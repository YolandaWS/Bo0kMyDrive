from django.shortcuts import render
from django.http import HttpResponseRedirect

#methodview
def Home(request):
    return render(request,'Home.html')

def BookACar(request):
    return render(request,'BookACar.html')

def About(request):
    return render(request,'About.html')


