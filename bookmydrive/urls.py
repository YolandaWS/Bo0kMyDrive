from django.contrib import admin
from django.urls import path,include
from .views import Home,BookACar,About
from SignIn.views import register
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Home,name='Home'),
    path('BookACar/', BookACar,name='BookACar'),
    path('Reviews/', include('Reviews.urls')),
    path('About/', About,name='About'),
    path('Articles/', include('Articles.urls')),
    path('SignIn/',register,name='SignIn'),
    path('LogIn/',auth_views.LoginView.as_view(template_name='SignIn/LogIn.html'),name='LogIn'),
    path('LogOut/',auth_views.LogoutView.as_view(template_name='SignIn/LogOut.html'),name='LogOut'),
    path('FavoriteCar/', include('FavoriteCar.urls')),
    path('GoogleAcc/',include('allauth.urls'))
]
